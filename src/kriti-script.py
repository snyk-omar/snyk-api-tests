import requests


ORG_ID = "3705094f-2138-46c5-a54f-bf9ebf9d7c3f"
TOKEN = "none of your damn business"

headers = {"Content-Type": "application/json", "Authorization": "token " + TOKEN}

get_projects_url = f"https://snyk.io/api/v1/org/{ORG_ID}/projects"

r = requests.post(get_projects_url, headers=headers)
json_res = r.json()

project_ids = []

for res in json_res["projects"]:
    # Any type of project that is not a SAST project, add to the list
    if res["type"] != "sast":
        project_ids.append(res["id"])

for project in project_ids:
    delete_projects_url = f"https://snyk.io/api/v1/org/{ORG_ID}/project/{project}"
    print(f"This project will be deleted: {delete_projects_url}")

    request = requests.delete(delete_projects_url, headers=headers)

# Check if the project was deleted
for id in project_ids:
    get_project_url = f"https://snyk.io/api/v1/org/{ORG_ID}/project/{id}"
    print(get_project_url)
    r = requests.post(get_project_url, headers=headers)
    if r.status_code == 404:
        print(f"Project {id} was deleted")
